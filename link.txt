https://drive.google.com/drive/folders/1R_ev78Ym3H0xJsyC71VepU6bP2CCck7T
https://mfikri.com/artikel/tutorial-codeigniter4
https://mfikri.com/artikel/login-register-codeigniter4

CREATE VIEW jumlah_ruang as
SELECT COUNT(nama_lokasi) AS jumlah_ruang FROM `lokasi`

CREATE VIEW jumlah_ruang_tiap_penanggungjawab AS
SELECT penanggung_jawab, COUNT(penanggung_jawab) AS jumlah_lokasi_penaggung_jawab FROM `lokasi`
GROUP BY penanggung_jawab

CREATE VIEW jumlah_ruang_tiap_penanggungjawab AS
SELECT penanggung_jawab as nama_penanggung_jawab, COUNT(penanggung_jawab) as jumlah_lokasi_penanggung_jawab FROM `lokasi` GROUP BY penanggung_jawab;

CREATE VIEW stok_terbanyak as 
SELECT id_barang, MAX(total_barang) FROM `stok`

CREATE VIEW jumlah_barang_lokasi AS
SELECT lokasi, SUM(jumlah_barang) AS total_barang FROM `barang` GROUP BY lokasi;

CREATE VIEW rata_rata_barang_masuk AS
SELECT AVG(jml_masuk) FROM `barang_masuk`

CREATE VIEW jumlah_barang_dipinjam_tiap_ruang AS
SELECT l.id_lokasi, l.nama_lokasi, b.jumlah_barang as jumlah_barang_dilokasi, p.barang_pinjam as id_barang, COUNT(b.nama_barang) as jumlah_peminjaman, SUM(p.jml_pinjam) as total_barang_dipinjam FROM `pinjam_barang` AS p
INNER JOIN  barang AS b ON b.id_barang = p.barang_pinjam
INNER  JOIN lokasi as l ON b.lokasi = l.id_lokasi
GROUP BY p.barang_pinjam HAVING COUNT(nama_barang) > 1

DELIMITER ||
CREATE FUNCTION newkodesupplier()
RETURNS CHARACTER(8)
BEGIN
    DECLARE kode_lama CHARACTER(8) DEFAULT 'SPR001';
    DECLARE ambil_angka INT DEFAULT 0;
    DECLARE kode_baru CHARACTER(8) DEFAULT 'SPR001';
    SELECT MAX(id_supplier) INTO kode_lama FROM supplier;
    SET ambil_angka = SUBSTR(kode_lama,4,6);
    SET ambil_angka = ambil_angka + 1;
    
    IF ambil_angka >= 10 THEN
    SET kode_baru = CONCAT('SPR0',ambil_angka);
    ELSE SET kode_baru = CONCAT('SPR00', ambil_angka);
    END IF;
    RETURN kode_baru;
END ||
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE tambah_barangmasuk(in_idbarang char(8),in_jmlmasuk int(11),in_idsupplier char(8))
BEGIN
	DECLARE total_seluruh INT DEFAULT 0;
	INSERT INTO barang_masuk (id_barang, tgl_masuk, jml_masuk, supplier)
    VALUES (in_idbarang, NOW(), in_jmlmasuk, in_idsupplier);
    UPDATE stok SET stok.jml_masuk = stok.jml_masuk + in_jmlmasuk, 
    				stok.total_barang = stok.jml_masuk - stok.jml_keluar
    	WHERE id_barang = in_idbarang;
    SELECT stok.total_barang INTO total_seluruh FROM stok 
    	WHERE id_barang = in_idbarang;
    UPDATE barang SET barang.jumlah_barang = total_seluruh 
    	WHERE id_barang = in_idbarang;
END $$
DELIMITER ;

DELIMITER ||
CREATE FUNCTION newkodebarang()
RETURNS CHARACTER(8)
BEGIN
	DECLARE kode_lama CHARACTER(8) DEFAULT 'SPR001';
    DECLARE ambil_angka INT DEFAULT 0;
    DECLARE kode_baru CHARACTER(8) DEFAULT 'SPR001';
    SELECT MAX(id_supplier) INTO kode_lama FROM supplier;
    SET ambil_angka = SUBSTR(kode_lama,4,6);
    SET ambil_angka = ambil_angka + 1;
    
    IF ambil_angka >= 10 THEN
    SET kode_baru = CONCAT('SPR0',ambil_angka);
    ELSE SET kode_baru = CONCAT('SPR00', ambil_angka);
    END IF;
    RETURN kode_baru;
END ||
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE pinjam_barang(in_idpeminjam char(8),in_idbarangpinjam char(8),in_jmlpinjam int)
BEGIN
	DECLARE stok_barang_pinjam INT DEFAULT 0;
    DECLARE total_akhir INT DEFAULT 0;
    	#isi total_barang
    SELECT jumlah_barang INTO stok_barang_pinjam FROM barang
    WHERE id_barang = in_idbarangpinjam;
    
     #apakah stok memenuhi?
    IF stok_barang_pinjam > in_jmlpinjam THEN
    	#input ke tabel pinjam
    	INSERT INTO pinjam_barang 
        (peminjam, tgl_pinjam, barang_pinjam, jml_pinjam, kondisi)
        VALUES 
        (in_idpeminjam, NOW(), in_idbarangpinjam, in_jmlpinjam, 'Baik');
        
        #update tabel stok
        UPDATE stok
        SET stok.total_barang = stok.total_barang - in_jmlpinjam
        WHERE id_barang = in_idbarangpinjam;

        #masukan total akhir
        SELECT total_barang INTO total_akhir FROM stok
        WHERE id_barang = in_idbarangpinjam;
        
        #update table barang
        UPDATE barang 
        SET barang.jumlah_barang = total_akhir
        WHERE id_barang = in_idbarangpinjam;
        
        ELSE SELECT "hai kamu gagal";
        END IF;
END $$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER triger_insert_barang_stok_akmal_xiirplb 
AFTER INSERT ON barang FOR EACH ROW
BEGIN
	INSERT INTO stok (id_barang,jml_masuk,total_barang) VALUES(new.id_barang, new.jumlah_barang, new.jumlah_barang);
END $$
DELIMITER ;

INSERT INTO barang(id_barang,nama_barang,lokasi,kondisi,jumlah_barang,sumber_dana)
VALUES (newkodebarang(), 'Proyektor Desk JET', 'R001', 'Baik', 10, 'S005')

DELIMITER $$
CREATE TRIGGER triger_log_insert_barang_akmal_xiirplb 
AFTER INSERT ON barang FOR EACH ROW
BEGIN
	INSERT INTO barang_log (nama_event,nama_barang,spesifikasi,lokasi,kondisi,jumlah_barang,sumber_dana,waktu_event) VALUES('tambah', new.nama_barang, new.spesifikasi,new.lokasi,new.kondisi,new.jumlah_barang,new.sumber_dana, NOW());
END $$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER triger_log_update_barang_akmal_xiirplb 
AFTER UPDATE ON barang FOR EACH ROW
BEGIN
	INSERT INTO barang_log (nama_event,nama_barang,spesifikasi,lokasi,kondisi,jumlah_barang,sumber_dana,waktu_event) VALUES('edit', new.nama_barang, new.spesifikasi,new.lokasi,new.kondisi,new.jumlah_barang,new.sumber_dana, NOW());
END $$
DELIMITER ;